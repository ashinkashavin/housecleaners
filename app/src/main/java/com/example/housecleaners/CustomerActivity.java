package com.example.housecleaners;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.io.ByteArrayOutputStream;

import java.util.ArrayList;
import java.util.List;

import android.widget.AdapterView;

public class CustomerActivity extends AppCompatActivity  implements AdapterView.OnItemSelectedListener  {
    private static String value;
    public static String getValue() {
        return value;
    }
    TextView uN, floorType;
    ImageView ImageViewSave,ImageViewCamera,ImageViewGallery ;
    Button ButtonSave,ButtonSubmit;
    EditText EditTextId,EditTextName,EditTextPhNumber,EditTextAddress,EditTextDate,EditTextTime,EditTextRooms,EditTextBathrooms;
Spinner SpinnerTextType;

   private DBHelper dbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);


        dbHelper=new DBHelper(this);
        dbHelper.OpenDB();

        ImageViewSave = (ImageView) findViewById(R.id.imageViewActivityProfilePicture);
        uN = findViewById(R.id.textView17);
        floorType = findViewById(R.id.textView23);
        EditTextId=(EditText) findViewById(R.id.txt_CUS_Id);
        EditTextName=(EditText) findViewById(R.id.txt_CUS_Name);
        EditTextPhNumber=(EditText)findViewById(R.id.txt_CUS_Number);
        EditTextAddress=(EditText) findViewById(R.id.txt_CUS_Address);
        EditTextDate=(EditText) findViewById(R.id.txt_CUS_Date);
        EditTextTime=(EditText) findViewById(R.id.txt_CUS_Hours);
        EditTextBathrooms=(EditText)findViewById(R.id.txt_CUS_Bathrooms);
        EditTextRooms=(EditText)findViewById(R.id.txt_CUS_Rooms);
        SpinnerTextType=(Spinner) findViewById(R.id.sp_CUS_Floor_type);
        ButtonSave = (Button) findViewById(R.id.btn_CUS_Save);
       ButtonSubmit=(Button) findViewById(R.id.btn_CUS_submit);

        String name = getIntent().getStringExtra("UserId");
        uN.setText(name);

        List<String> FloorType = new ArrayList<String>();
        FloorType.add("Cement Floor");
        FloorType.add("Tile Floor");
        FloorType.add("Wooden Floor");


        // Creating adapter for spinner
        ArrayAdapter<String> TypeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, FloorType);
        // Drop down layout style - list view with radio button
        TypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        SpinnerTextType.setAdapter(TypeAdapter);
        SpinnerTextType.setOnItemSelectedListener(this);

        ButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (EditTextId.getText().toString().isEmpty()||
                        EditTextName.getText().toString().isEmpty() ||
                        EditTextPhNumber.getText().toString().isEmpty()||
                        EditTextAddress.getText().toString().isEmpty()||
                        EditTextDate.getText().toString().isEmpty()||
                EditTextTime.getText().toString().isEmpty()||
                EditTextRooms.getText().toString().isEmpty()||
                EditTextBathrooms.getText().toString().isEmpty()||
                        SpinnerTextType.toString().isEmpty())


                {
                    Toast.makeText(getApplicationContext(),"Fields cant be blank",Toast.LENGTH_LONG).show();
                }
                else
                {
                    String FloorType = SpinnerTextType.getSelectedItem().toString();
                    BookingDetails bookingDetails = new BookingDetails(EditTextId.getText().toString(),
                            Integer.parseInt(EditTextPhNumber.getText().toString()),
                            EditTextName.getText().toString(),
                            EditTextAddress.getText().toString(),
                            EditTextDate.getText().toString(),
                            EditTextTime.getText().toString(),
                            EditTextRooms.getText().toString(),
                            EditTextBathrooms.getText().toString(),
                            FloorType);


                    if (dbHelper.InsertBookingDetails( bookingDetails)){
                        Toast.makeText(getApplicationContext(),"Your Information is recorded ",Toast.LENGTH_LONG).show();
                        value = EditTextId.getText().toString().trim();
                        Intent intentCustomer= new Intent(CustomerActivity.this,CreatePostActivity.class);
                        startActivity(intentCustomer);

                    }else {

                        Toast.makeText(getApplicationContext(),"Failed",Toast.LENGTH_LONG).show();


                    }

                }
            }
        });



        ImageViewSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChooseProfilePicture();
            }

        });


        ButtonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                byte[] byteSSP = covertImageToByteArray(ImageViewSave);
                //dbHelper=new DBConnector(MainActivity.this);

                String BDId= (EditTextId.getText().toString());
                if (dbHelper.save(BDId,byteSSP))
                {
                    Toast.makeText(getApplicationContext(), "save", Toast.LENGTH_LONG).show();
                } else {

                    Toast.makeText(getApplicationContext(), "Not save ", Toast.LENGTH_LONG).show();
                }

            }
        });

    }


    private byte[] covertImageToByteArray(ImageView imageView)
    {
        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();

    }

    private void ChooseProfilePicture()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(CustomerActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_dialog_picture, null);
        builder.setCancelable(false);
        builder.setView(dialogView);

        ImageViewCamera = dialogView.findViewById(R.id.imageViewDPPCamera);
        ImageViewGallery = dialogView.findViewById(R.id.imageViewDPPGallery);
        final AlertDialog alertDialogProfilePicture = builder.create();
        alertDialogProfilePicture.show();


        ImageViewCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkAndRequestPermission()) {
                    takePictureFromCamera();
                    alertDialogProfilePicture.cancel();
                }
            }
        });

        ImageViewGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePictureFromGallery();
            }
        });
    }

    private void takePictureFromGallery()
    {
        Intent pickPhoto=new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto,1);

    }
    private void takePictureFromCamera()
    {

        Intent takePicture=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePicture.resolveActivity(getPackageManager())!=null)
        {
            startActivityForResult(takePicture,2);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case 1:
                if(resultCode==RESULT_OK)
                {
                    Uri selectImageUri=data.getData();
                    ImageViewSave.setImageURI(selectImageUri);
                }
                break;
            case 2:
                if(resultCode==RESULT_OK)
                {
                    Bundle bundle=data.getExtras();
                    Bitmap bitmapImage=(Bitmap) bundle.get("data");
                    ImageViewSave.setImageBitmap(bitmapImage);
                }
                break;
        }
    }

    private boolean checkAndRequestPermission()
    {
        if(Build.VERSION.SDK_INT>23)
        {
            int  cameraPermission= ActivityCompat.checkSelfPermission(CustomerActivity.this, Manifest.permission.CAMERA);
            if(cameraPermission== PackageManager.PERMISSION_DENIED)
            {
                ActivityCompat.requestPermissions(CustomerActivity.this,new String[]{Manifest.permission.CAMERA},20);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String choice = parent.getItemAtPosition(position).toString();
        floorType.setText(choice);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}