package com.example.housecleaners;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class CleanLogin extends AppCompatActivity {
    EditText EditTextUserId,EditTextPassword;
    Button ButtonLogin;

    private DBHelper dbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clean_login);

        dbHelper =new DBHelper(this);
        dbHelper .OpenDB();

        EditTextUserId=(EditText) findViewById(R.id.txt_L_UserId);
        EditTextPassword=(EditText) findViewById(R.id.txt_L_Password);

        ButtonLogin=(Button) findViewById(R.id.btn_L_Login);

        ButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<UserInfo> userDetails=dbHelper.ValidLogin(EditTextUserId.getText().toString(),EditTextPassword.getText().toString());
                if (userDetails.size()!=0)
                {
                    UserInfo userInfo=userDetails.get(0);
                    String UserType=userInfo.getUserType();

                    Toast.makeText(getApplicationContext(), "user found"+UserType, Toast.LENGTH_SHORT).show();
                    if (UserType.equals("Cleaner"))
                    {
                        Intent intentCustomer= new Intent(CleanLogin.this,CleanerButtonActivity.class);
                        startActivity(intentCustomer);
                    }
                    else
                    {
                        Intent intentCleaner= new Intent(CleanLogin.this,CustomerButtonActivity.class);
                        startActivity(intentCleaner);

                    }


                }

                else{
                    Toast.makeText(getApplicationContext(), "Invalid User", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }
}