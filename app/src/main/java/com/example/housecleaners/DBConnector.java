package com.example.housecleaners;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBConnector extends SQLiteOpenHelper
{
public DBConnector(Context context){
    super(context,"DB_129",null,1);

}

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table UserInfo (UserID VARCHAR PRIMARY KEY NOT NULL,Password VARCHAR,UserType VARCHAR)");
        db.execSQL("create table BookingDetails(BDId VARCHAR PRIMARY KEY ,Phnumber INTERGER , Name VARCHAR ,Address VARCHAR,Date VARCHAR,Hours VARCHAR,Rooms VARCHAR,Bathrooms VARCHAR,FloorType VARCHAR)");
        db.execSQL("create table Price(PriceId INTEGER PRIMARY KEY AUTOINCREMENT,Price VARCHAR,BDId VARCHAR)");
        db.execSQL("create table Image(ImageId INTEGER PRIMARY KEY AUTOINCREMENT,PP BLOB,BDId VARCHAR)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
