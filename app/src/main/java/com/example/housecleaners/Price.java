package com.example.housecleaners;

public class Price {

    private  int PriceId;
    private String Price;
    private String BDId;

    public Price( String price,String BDId) {
       this.Price = price;
        this.BDId = BDId;
    }


    public int getPriceId() {
        return PriceId;
    }

    public void setPriceId(int priceId) {
        PriceId = priceId;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getBDId() {
        return BDId;
    }

    public void setBDId(String BDId) {
        this.BDId = BDId;
    }
}
