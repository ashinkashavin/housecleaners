package com.example.housecleaners;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CustomerButtonActivity extends AppCompatActivity {

Button ButtonDetails,Buttonfeedback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_button);

        ButtonDetails=(Button) findViewById(R.id.btn_CB_DETAILS);
        Buttonfeedback=(Button) findViewById(R.id.btn_CB_Price);

        ButtonDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLogin=new Intent(CustomerButtonActivity.this,CustomerActivity.class);
                startActivity(intentLogin);
            }
        });

        Buttonfeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRegister=new Intent(CustomerButtonActivity.this,FeedbackActivity.class);
                startActivity(intentRegister);
            }
        });


    }
}