package com.example.housecleaners;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;

public class CreatePostActivity extends AppCompatActivity {

    TextView TextViewId, TextViewName,TextViewPhNumber,TextViewAddress,TextViewDate,TextViewTextTime,TextViewNoRooms, TextViewNoBathrooms, TextViewFloorType, TextViewroom, TextViewbathRoom, TextViewfloor, TextViewprice;
    Button backBtn,btnSubmit;

    private DBHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);

        dbHelper=new DBHelper(this);
        dbHelper.OpenDB();


        TextViewId=(TextView) findViewById(R.id.textViewHouseId);
        TextViewPhNumber=(TextView) findViewById(R.id.textViewPhNumber);
        TextViewAddress=(TextView) findViewById(R.id.textViewAddress);
        TextViewDate=(TextView) findViewById(R.id.textViewDate);
        TextViewTextTime=(TextView) findViewById(R.id.textViewTime);
        TextViewName = findViewById(R.id.textViewName);
        backBtn = findViewById(R.id.btn_C_back);
        btnSubmit=findViewById(R.id.btn_C_Submit);
        TextViewNoRooms = findViewById(R.id.textViewRoom);
        TextViewNoBathrooms = findViewById(R.id.textViewBathroom);
        TextViewFloorType = findViewById(R.id.textViewFloorType);
        TextViewroom = findViewById(R.id.textViewRoom1);
        TextViewbathRoom = findViewById(R.id.textViewBathroom1);
        TextViewfloor = findViewById(R.id.textViewFloorType1);
        TextViewprice = findViewById(R.id.textViewPrice);

        backBtn.setBackgroundColor(Color.BLUE);
        TextViewId.setText(CustomerActivity.getValue());


        TextViewroom.setText("No Of Rooms");
        TextViewbathRoom.setText("No Of Bathrooms");
        TextViewfloor.setText("FloorType");

        String BDId = TextViewId.getText().toString();

            Cursor cursor = dbHelper.viewHouse(BDId);
        if (cursor.moveToNext()){
            TextViewPhNumber.setText(cursor.getString(1));
            TextViewName.setText(cursor.getString(2));
            TextViewAddress.setText(cursor.getString(3));
            TextViewDate.setText(cursor.getString(4));
            TextViewTextTime.setText(cursor.getString(5));
            TextViewNoRooms.setText(cursor.getString(6));
            TextViewNoBathrooms.setText(cursor.getString(7));
            TextViewFloorType.setText(cursor.getString(8));

            String FT = TextViewFloorType.getText().toString();
            int nR = Integer.parseInt(TextViewNoRooms.getText().toString());
            int nBr = Integer.parseInt(TextViewNoBathrooms.getText().toString());
            switch (FT) {
                case "Cement Floor": {
                    int p1 = nR * 500;
                    int p2 = nBr * 300;
                    int pF = p1 + p2;
                    TextViewprice.setText(String.valueOf(pF));
                    break;
                }
                case "Tile Floor": {
                    int p1 = nR * 450;
                    int p2 = nBr * 220;
                    int pF = p1 + p2;
                    TextViewprice.setText(String.valueOf(pF));
                    break;
                }
                case "Wooden Floor": {
                    int p1 = nR * 400;
                    int p2 = nBr * 200;
                    int pF = p1 + p2;
                    TextViewprice.setText(String.valueOf(pF));
                    break;
                }
            }
        }
        else {
            Toast.makeText(this, "Cannot find House details. So please enter house details", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, CustomerActivity.class);
            startActivity(intent);
        }

        backBtn.setOnClickListener(v -> {
            Intent intent = new Intent(this, CleanLogin.class);
            startActivity(intent);
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Price price = new Price(TextViewprice.getText().toString(),TextViewId.getText().toString());


                if (dbHelper.InsertPriceDetails(price)){
                    Toast.makeText(getApplicationContext(),"Your Information is recorded ",Toast.LENGTH_LONG).show();

                }
            }
        });


    }

}