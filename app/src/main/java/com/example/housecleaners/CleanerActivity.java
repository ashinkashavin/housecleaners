package com.example.housecleaners;


import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class CleanerActivity extends AppCompatActivity {
    ListView ListViewProducts;
    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cleaner);


        dbHelper=new DBHelper(this);
        dbHelper.OpenDB();
        ListViewProducts=(ListView) findViewById(R.id.lst_L_Products);

        ArrayList<BookingDetails> theList=new ArrayList<>();
        Cursor cursor = dbHelper.SearchAllProduct();
        if (cursor.getCount()==0){
            Toast.makeText(getApplicationContext(),"No data found",Toast.LENGTH_LONG).show();

        }
        else{

            while (cursor.moveToNext())
            {
                theList.add(new BookingDetails(cursor.getString(0), cursor.getString(1)));
                ListAdapter listAdapter=new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,theList);
                ListViewProducts.setAdapter(listAdapter);
            }
        }
        ListViewProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                BookingDetails bd = theList.get(position);
                ArrayList<BookingDetails> bookingDetailsList=dbHelper.SearchDetails(bd.getBDId());
                if (bookingDetailsList.size()!=0)
                {
                    BookingDetails bookingDetails =bookingDetailsList.get(0);
                    Intent intentCleanerActivity = new Intent(CleanerActivity.this,ViewProductActivity.class);
                    intentCleanerActivity.putExtra("BDId",bookingDetails.getBDId());
                    intentCleanerActivity.putExtra("Phnumber",String.valueOf(bookingDetails.getPhnumber()));
                    intentCleanerActivity.putExtra("Name",bookingDetails.getName());
                    intentCleanerActivity.putExtra("Address",bookingDetails.getAddress());
                    intentCleanerActivity.putExtra("Date",bookingDetails.getDate());
                    intentCleanerActivity.putExtra("Hours",bookingDetails.getHours());
                    intentCleanerActivity.putExtra("Rooms",bookingDetails.getRooms());
                    intentCleanerActivity.putExtra("Bathrooms",bookingDetails.getBathrooms());
                    intentCleanerActivity.putExtra("FloorType",bookingDetails.getFloorType());
                    intentCleanerActivity.putExtra("Price",bookingDetails.getPrice().getPrice());
                    startActivity(intentCleanerActivity);
                }
            }
        });


    }
}