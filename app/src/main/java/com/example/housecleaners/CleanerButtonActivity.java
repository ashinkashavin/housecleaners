package com.example.housecleaners;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class CleanerButtonActivity extends AppCompatActivity {
Button ButtonDetails,Buttonfeedback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cleaner_button);

        ButtonDetails=(Button) findViewById(R.id.btn_cl_view);
        Buttonfeedback=(Button) findViewById(R.id.btn_cl_Feedback);


        ButtonDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLogin=new Intent(CleanerButtonActivity.this,CleanerActivity.class);
                startActivity(intentLogin);
            }
        });

        Buttonfeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRegister=new Intent(CleanerButtonActivity.this,FeedbackActivity.class);
                startActivity(intentRegister);
            }
        });


    }
}