package com.example.housecleaners;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ImageviewActivity extends AppCompatActivity {
    ImageView ImageViewShow1,ImageViewShow2,ImageViewShow3,ImageViewShow4,ImageViewShow5,ImageViewShow6;
    Button ButtonBack;
    private DBHelper dbHelper;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imageview);

        dbHelper=new DBHelper(this);
        dbHelper.OpenDB();
        ImageViewShow1=(ImageView)findViewById(R.id.imageView2);
        ImageViewShow2=(ImageView)findViewById(R.id.imageView3);
        ImageViewShow3=(ImageView)findViewById(R.id.imageView4);
        ImageViewShow4=(ImageView)findViewById(R.id.imageView5);
        ImageViewShow5=(ImageView)findViewById(R.id.imageView6);
        ImageViewShow6=(ImageView)findViewById(R.id.imageView7);
        ButtonBack=(Button)findViewById(R.id.button2);

        String BDId=this.getIntent().getStringExtra("BDId");
        byte[][] images=dbHelper.getImage(BDId);
        for(int i=0;i<images.length;i++){
            Bitmap bitmap = coverByteArrayToBitmap(images[i]);
           switch(i+1){
               case 1:
               default:
                   ImageViewShow1.setImageBitmap(bitmap);
                   break;
               case 2:
                   ImageViewShow2.setImageBitmap(bitmap);
                   break;
               case 3:
                   ImageViewShow3.setImageBitmap(bitmap);
                   break;
               case 4:
                   ImageViewShow4.setImageBitmap(bitmap);
                   break;
               case 5:
                   ImageViewShow5.setImageBitmap(bitmap);
                   break;
               case 6:
                   ImageViewShow6.setImageBitmap(bitmap);
                   break;
           }
        }


        ButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCustomer= new Intent(ImageviewActivity.this,CleanerActivity.class);
                startActivity(intentCustomer);
            }
        });
    }
    private Bitmap coverByteArrayToBitmap(byte[]bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }
}