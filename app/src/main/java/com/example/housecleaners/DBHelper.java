package com.example.housecleaners;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class DBHelper {
    private Context con;
    private SQLiteDatabase db;

    public DBHelper(Context con){
        this.con=con;

    }
    public DBHelper OpenDB(){
        DBConnector dbCon = new DBConnector(con);
        db = dbCon.getWritableDatabase();
        db = dbCon.getReadableDatabase();
        return this;


    }
    public boolean insertUser(UserInfo userInfo){
        try{
            db.execSQL("insert into UserInfo values('"+userInfo.getUserId()+"','"+userInfo.getPassword()+"','"+userInfo.getUserType()+"')");
            return true;

        }
        catch(Exception ex){
            ex.printStackTrace();
            return false;

        }


    }

    public ArrayList<UserInfo>ValidLogin(String UserId,String Password)
    {
        ArrayList<UserInfo> userList=new ArrayList<UserInfo>();
        try{
            Cursor cursor= db.rawQuery("Select * from UserInfo where UserID='"+UserId+"'and Password='"+Password+"'",null);
            if (cursor.moveToFirst())
            {
                do {
                    UserInfo user=new UserInfo();
                    user.setUserId(cursor.getString(0));
                    user.setPassword(cursor.getString(1));
                    user.setUserType(cursor.getString(2));
                   userList.add(user);
                }while (cursor.moveToNext());

            }


        }
        catch(Exception ex)
        {
            ex.printStackTrace();

        }
        return userList;


    }


    public boolean save(String BDId,byte[] pp)
    {
        try
        {
            ContentValues cv=new ContentValues();
            cv.put("BDId",BDId);
            cv.put("pp",pp);

            //  SQLiteDatabase db=this.getWritableDatabase();
            db.insert("Image",null,cv);

            return  true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }
    public boolean InsertBookingDetails(BookingDetails bookingDetails)
    {
        try{
            db.execSQL("insert into BookingDetails values ('"+bookingDetails.getBDId()+"','"+bookingDetails.getPhnumber()+"','"+bookingDetails.getName()+"','"+bookingDetails.getAddress()+"','"+bookingDetails.getDate()+"','"+bookingDetails.getHours()+"','"+bookingDetails.getRooms()+"','"+bookingDetails.getBathrooms()+"','"+bookingDetails.getFloorType()+"')");
            return  true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }

    }
    public boolean InsertPriceDetails(Price price)
    {
        try{
            db.execSQL("insert into Price (Price,BDId)Values('"+price.getPrice()+"','"+price.getBDId()+"')");
            return  true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }

    }

    public ArrayList<BookingDetails> SearchDetails(String BDId)
    {
        ArrayList<BookingDetails> bookingList=new ArrayList<BookingDetails>();
        try
        {
            Cursor cursor=db.rawQuery("Select b.*, p.price from BookingDetails b JOIN Price p ON p.BDId=b.BDId where b.BDId='"+BDId+"' ",null);
            if(cursor.moveToNext()){
                do {
                    BookingDetails bookingDetails=new BookingDetails();
                    bookingDetails.setBDId(cursor.getString(0));
                    bookingDetails.setPhnumber(cursor.getInt(1));
                    bookingDetails.setName(cursor.getString(2));
                    bookingDetails.setAddress(cursor.getString(3));
                    bookingDetails.setDate(cursor.getString(4));
                    bookingDetails.setHours(cursor.getString(5));
                    bookingDetails.setRooms(cursor.getString(6));
                    bookingDetails.setBathrooms(cursor.getString(7));
                    bookingDetails.setFloorType(cursor.getString(8));
                    Price bookingPrice = new Price(cursor.getString(9),cursor.getString(0));
                    bookingDetails.setPrice(bookingPrice);
                    bookingList.add(bookingDetails);

                }while (cursor.moveToNext());

            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Toast.makeText(con, ex.getMessage(),Toast.LENGTH_LONG).show();
        }
        return bookingList;
    }

    public Cursor SearchAllProduct(){
        Cursor cursor=null;
        try
        {
            cursor=db.rawQuery("Select BDId, Name from BookingDetails ",null);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return cursor;

    }

    public byte[][] getImage(String BDId)
    {
        try {
            //SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery("Select PP from Image where BDId='"+BDId+"'", null);
           byte[][] images = new byte[cursor.getCount()][];
           if(cursor.moveToNext()) {
                do{
                    Array.set(images,cursor.getPosition(),cursor.getBlob(0));
                }
                while(cursor.moveToNext());
                return images;
            }
            return new byte[0][];
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return new byte[0][];
        }
    }
    public Cursor viewHouse (String BDId) {
        DBConnector dbCon = new DBConnector(con);
        db = dbCon.getReadableDatabase();
        Cursor cursor=db.rawQuery("Select* from BookingDetails where BDId='"+BDId+"' ",null);
        return cursor;

    }

}


