package com.example.housecleaners;

public class BookingDetails {

         private String BDId;
         private int Phnumber;
         private String Name;
         private String Address;
         private String Date;
         private String Hours;
         private String Rooms;
         private String Bathrooms;
         private String FloorType;
         private Price price;
         private byte[] image;

    public BookingDetails(String BDId, int phnumber, String name, String address, String date, String hours, String rooms, String bathrooms, String floorType) {
        this.BDId = BDId;
        Phnumber = phnumber;
        Name = name;
        Address = address;
        Date = date;
        Hours = hours;
        Rooms = rooms;
        Bathrooms = bathrooms;
        FloorType = floorType;
    }

    public BookingDetails() {
    }

    public BookingDetails(String BDId, String Name) {
        this.setBDId(BDId);
        this.setName(Name);
    }

    public String getBDId() {
        return BDId;
    }

    public void setBDId(String BDId) {
        this.BDId = BDId;
    }

    public int getPhnumber() {
        return Phnumber;
    }

    public void setPhnumber(int phnumber) {
        Phnumber = phnumber;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getHours() {
        return Hours;
    }

    public void setHours(String hours) {
        Hours = hours;
    }

    public String getRooms() {
        return Rooms;
    }

    public void setRooms(String rooms) {
        Rooms = rooms;
    }

    public String getBathrooms() {
        return Bathrooms;
    }

    public void setBathrooms(String bathrooms) {
        Bathrooms = bathrooms;
    }

    public String getFloorType() {
        return FloorType;
    }

    public void setFloorType(String floorType) {
        FloorType = floorType;
    }

    public Price getPrice() {
        return this.price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public byte[] getImage() {
        return image;
    }

    public byte[] setImage(byte[] image) {
        this.image = image;
        return image;
    }

    public String toString() {
        return this.getName();
    }
}


