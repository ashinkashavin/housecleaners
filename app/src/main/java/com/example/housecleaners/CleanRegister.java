package com.example.housecleaners;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class CleanRegister extends AppCompatActivity {
    Button ButtonRegister;
    EditText EditTextUserName,EditTextPassword,EditTextConfirmPassword;
    Spinner SpinnerUserType;

    private DBHelper dbHelper;

    String UserType[]={"Cleaner","Customer"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clean_register);

        dbHelper=new DBHelper(this);
        dbHelper.OpenDB();

        EditTextUserName=(EditText) findViewById(R.id.txt_R_Userid);
        EditTextPassword=(EditText) findViewById(R.id.txt_R_Password);
        EditTextConfirmPassword=(EditText) findViewById(R.id.txt_R_ConfirmPassword);
        ButtonRegister=(Button) findViewById(R.id.btn_R_Register);

        SpinnerUserType=(Spinner) findViewById(R.id.sp_R_UserType);

        ArrayAdapter ad=new ArrayAdapter(this, android.R.layout.simple_spinner_item,UserType);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpinnerUserType.setAdapter(ad);

        ButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (EditTextUserName.getText().toString().isEmpty() || EditTextPassword.getText().toString().isEmpty()||EditTextConfirmPassword.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Fields cant be blank", Toast.LENGTH_SHORT).show();

                }
                else if(EditTextPassword.getText().toString().length()<8){
                    Toast.makeText(getApplicationContext(), "Password must have more than 8 characters", Toast.LENGTH_SHORT).show();

                }
                else if (!EditTextPassword.getText().toString().equals(EditTextConfirmPassword.getText().toString()))
                {
                    Toast.makeText(getApplicationContext(), "Password and confirm password should match to login", Toast.LENGTH_SHORT).show();

                }
                else
                {
                    UserInfo userInfo=new UserInfo(EditTextUserName.getText().toString(),EditTextPassword.getText().toString(),SpinnerUserType.getSelectedItem().toString());
                    if (dbHelper.insertUser(userInfo)){
                        Toast.makeText(getApplicationContext(),"User created",Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(),  EditTextUserName.getText().toString()+"has Registered as" +SpinnerUserType.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();

                    }
                    else{
                        Toast.makeText(getApplicationContext(), "User creation Failed", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });
    }
}