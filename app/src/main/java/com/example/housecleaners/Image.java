package com.example.housecleaners;

public class Image {

   private int ImageId;
    private byte[] image;
    private String BDId;

    public Image(int imageId, byte[] image, String BDId) {
        ImageId = imageId;
        this.image = image;
        this.BDId = BDId;
    }

    public Image() {
    }

    public int getImageId() {
        return ImageId;
    }

    public void setImageId(int imageId) {
        ImageId = imageId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getBDId() {
        return BDId;
    }

    public void setBDId(String BDId) {
        this.BDId = BDId;
    }
}
