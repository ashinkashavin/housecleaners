package com.example.housecleaners;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ViewProductActivity extends AppCompatActivity {
    TextView TextViewId,TextViewPhNumber,TextViewName,TextViewAddress,TextViewDate,TextViewHours,TextViewRoom,TextViewBathRoom,TextViewFloorType,TextViewPrice;
    ImageView ImageViewShow ;
    Button  ButtonShow,ButtonBack;
    EditText EditTextSearchImageId;
    private DBHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_product);
        dbHelper=new DBHelper(this);
        dbHelper.OpenDB();
        TextViewId=(TextView) findViewById(R.id.tv_VP_Id);
        TextViewPhNumber=(TextView) findViewById(R.id.tv_VP_PhNumber);
        TextViewName=(TextView) findViewById(R.id.tv_VP_Name);
        TextViewAddress=(TextView) findViewById(R.id.tv_VP_Address);
        TextViewDate=(TextView) findViewById(R.id.tv_VP_Date);
        TextViewHours=(TextView) findViewById(R.id.tv_VP_Hours);
        TextViewRoom=(TextView) findViewById(R.id.tv_VP_Room);
        TextViewBathRoom=(TextView) findViewById(R.id.tv_VP_BathRoom);
        TextViewFloorType=(TextView) findViewById(R.id.tv_VP_FloorType);
        TextViewPrice = (TextView) findViewById(R.id.tv_VP_Price);
        ButtonBack=(Button) findViewById(R.id.btn_V_Back);
        ButtonShow=(Button)findViewById(R.id.btn_V_Show);

         ButtonShow.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Intent intentView= new Intent(ViewProductActivity.this,ImageviewActivity.class);
                 intentView.putExtra("BDId",getIntent().getStringExtra("BDId"));
                 startActivity(intentView);
             }
         });

        ButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCustomer= new Intent(ViewProductActivity.this,CleanerButtonActivity.class);
                startActivity(intentCustomer);
            }
        });
        Intent intent= this.getIntent();

        TextViewId.setText(intent.getStringExtra("BDId"));
        TextViewPhNumber.setText(intent.getStringExtra("Phnumber"));
        TextViewName.setText(intent.getStringExtra("Name"));
        TextViewAddress.setText(intent.getStringExtra("Address"));
        TextViewDate .setText(intent.getStringExtra("Date"));
        TextViewHours.setText(intent.getStringExtra("Hours"));
        TextViewRoom .setText(intent.getStringExtra("Rooms"));
        TextViewBathRoom.setText(intent.getStringExtra("Bathrooms"));
        TextViewFloorType.setText(intent.getStringExtra("FloorType"));
        TextViewPrice.setText(intent.getStringExtra("Price"));

    }
    private Bitmap coverByteArrayToBitmap(byte[]bytes){
        return BitmapFactory.decodeByteArray(bytes,0,bytes.length);

    }
}